const axios = require("axios");

const args = require("minimist")(process.argv.slice(2));
let stack = [];
const domain = args._[0];
const fetch = async uri => {
  const response = await axios.get(uri);
  if (!response.data) throw response;
  return response.data;
};

const mapLinks = async (stack, domain) => {
  const currentUri = stack.pop();
  console.log(domain);
  const data = await fetch(domain);

  let hrefs = [...data.matchAll(/\<a.*?href="(.*?)"/gi)].map(href => href[1]);
  //.map(href => href.slicen(6, href.length - 1));
  //.filter(href => href.slice(0, 4) === "http");

  console.log(hrefs);
};

mapLinks(stack, domain).catch(err => console.log(err.response));
