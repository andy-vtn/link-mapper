module.exports.isLocal = link => {
  if (!link.includes("://") && !link.includes("www.")) return true;
  else return false;
};

module.exports.isHashLink = href => {
  if (href.includes("#")) return true;
  else return false;
};

module.exports.isNotMailTo = href => {
  return !href.includes("mailto:");
};
