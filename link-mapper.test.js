const linkMapper = require("./link-mapper");

describe("Normalize domain function", () => {
  test("It should do nothing if valid domain with slash at the end", () => {
    expect(linkMapper.normalize("https://gva.be/")).toEqual("https://gva.be/");
  });
  test("It should do nothing if a valid http domain was added.", () => {
    expect(linkMapper.normalize("http://gva.be/")).toEqual("http://gva.be/");
  });
  test("It should add a slash at the add if there is none", () => {
    expect(linkMapper.normalize("http://gva.be")).toEqual("http://gva.be/");
  });
});
