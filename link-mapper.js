const fetch = require("./fetch");

const args = require("minimist")(process.argv.slice(2));

const isLocal = require("./link-analyser").isLocal;
const isHashLink = require("./link-analyser").isHashLink;
const isNotMailTo = require("./link-analyser").isNotMailTo;
const hasFileInput = require("./html-analyser").hasFileInput;
const hasTextInput = require("./html-analyser").hasTextInput;

let domain;

const normalize = require("./url-helper").normalize;
const localToUrl = require("./url-helper").localToUrl;

const addToStack = (url, stack, visited) => {
  if (!visited.includes(url)) stack.push(url);
  return [...new Set(stack)];
};

const updateStack = (hrefs, stack, visited, currentUrl) => {
  hrefs
    .filter(isLocal)
    .filter(href => !isHashLink(href))
    .filter(isNotMailTo)
    .map(href => localToUrl(href, domain))
    .forEach(url => {
      stack = addToStack(url, stack, visited);
    });
  hrefs
    .filter(href => !isLocal(href))
    .filter(href => isInScope(domain, href))
    .forEach(url => (stack = addToStack(url, stack, visited)));
  return stack;
};

const isInScope = (scope, url) => url.includes(scope);

const mapLinks = async (stack = null, visited = []) => {
  domain = normalize(domain);
  let hrefs;
  if (!stack) {
    stack = [domain];
  }
  const currentUrl = stack.shift();
  visited.push(currentUrl);
  console.log("Stack:" + stack.length, "Visited:" + visited.length);
  try {
    const data = await fetch(currentUrl);
    hrefs = [...data.matchAll(/\<a.*?href="(.*?)"/gi)].map(href => href[1]);

    console.log(
      currentUrl,
      hasFileInput(data) ? "/FILE" : "",
      hasTextInput(data) ? "/TEXT" : ""
    );
    stack = updateStack(hrefs, stack, visited, currentUrl);
  } catch (err) {
    console.log(currentUrl);
    console.log("Error:" + err);
  }
  if (stack.length > 0) await mapLinks(stack, visited);
  else return visited;
};

domain = args._[0];
mapLinks()
  .then(urls => console.log(urls))
  .catch(err => console.log(err));

module.exports.normalize = normalize;
