module.exports.hasFileInput = html =>
  html.match(/\<input.*? type="file".*?\>/i) ? true : false;

module.exports.hasTextInput = html =>
  html.match(/\<input.*? type="text".*?\>/i) ? true : false;
