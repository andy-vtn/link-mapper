const request = require("request");

module.exports = async uri => {
  const options = {
    url: uri,
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:70.0) Gecko/20100101 Firefox/70.0",
      Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      Cookie:
        "SSLB=1; SSID=CABR4x2aAAAAAABzYNZdgV6BAHNg1l0LAAAAAABT439iydnkXQBUmhQDAAOnRQAAc2DWXQsAoAUAAQJwAADJ2eRdAQBrBQABc2wAAMnZ5F0BAKsFAAFxcAAAydnkXQEAkAUAA2FuAABzYNZdCwAhBAADFFsAACdc3V0DAGYFAAMjbAAAOeXYXQUAsAUAAaFwAADJ2eRdAQAIAwAD5EQAAHNg1l0LAK8FAAGccAAAydnkXQEASwUAAQ9rAADJ2eRdAQA; SSRT=ydnkXQADAA; SSPV=bI4AAAAAAA0ADwAAAAAAAAAAAAIAAAAAAAAAAAAA; __cfduid=d0cad8420f45e733b469f666e5f94b2841574330484; _ga=GA1.2.1645872571.1574330485; cookies-accepted=1.8; cookies-accepted=1.6; rxVisitor=1574330508423UDR2DMPM519KAISRJVMUHPTD04QUBFAH; Pastease.passive.chance.nKhwRH8veU46n3G=1; Pastease.passive.activated.nKhwRH8veU46n3G=0; __gads=ID=8e7c102ba5dd02ee:T=1574330638:S=ALNI_MYe3ZtxWL50gWiTGZDeb_TuE9d6IQ; _gcl_au=1.1.1058544234.1574330638; ah_token_presumed=1018679c-b724-4644-a180-da81fc2c84f7.b8ea34f9-f3ad-47fe-b505-1010da3ccabb; ahold_presumed_member_no=98671910_CHKE4D5g4FAWJA7KFrvYlB8kg%3D%3D; Pastease.passive.chance.dW9UvdwBfc8RQMk=1; Pastease.passive.activated.dW9UvdwBfc8RQMk=0; Pastease.passive.chance.KQwzQo32wEOrJRp=1; Pastease.passive.activated.KQwzQo32wEOrJRp=0; ui_scaling=size_s%20size_m%20size_l%20size_xl%20size_xxl; _vwo_uuid_v2=D4A64DE65DAD7C7E8C0A319248D79385F|ad7cb31838d177adae67062189a41027; _fbp=fb.1.1574461241006.219835995; SSSC=4.G6761697937589362305.11|776.17636:788.17831:1057.23316:1355.27407:1382.27683:1387.27763:1424.28257:1440.28674:1451.28785:1455.28828:1456.28833; TS01c61a60=01919b9b64855b0bc6eb8391c03a60e40d24058eb51257223f3f0a5c05789748736fbf6643035b97cc6b4d7e081df2bb797775f0f4edca963f6d19c573533e76260b6be9c318ef4640a1290ccc4ce013cc595f2820762c656695eddefb604897c0b069eed85242591212d3780cec974c0f3f93118c2bd2ea6d62a161300c0f7f199230eacbc6c3e550923308ee5c8082bd1ce781fb; _gid=GA1.2.983756312.1575273505; _gat_initialTracker=1; _gat_mainTracker=1"
    }
  };
  return new Promise((resolve, reject) => {
    request(options, (err, response, body) => {
      if (!err) resolve(body);
      else reject(err);
    });
  });
};
