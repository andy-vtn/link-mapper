module.exports.normalize = domain => {
  domain = !domain.match(/\/$/) ? domain + "/" : domain;
  if (domain.match(/^https*:\/\/.*\.[a-z]+\/$/)) return domain;
};

module.exports.localToUrl = (href, domain) => {
  if (href.charAt(0) === "/") return domain + href.slice(1, href.length);
  else return domain + href;
};
